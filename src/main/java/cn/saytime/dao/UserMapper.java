package cn.saytime.dao;

import cn.saytime.bean.User;
import java.util.List;
import org.apache.ibatis.annotations.Param;


public interface UserMapper {
    User getUserById(Integer id);

    public List<User> getUserList();

    public int add(User user);

    public int update(Integer id, User user);

    public int delete(Integer id);
}
