package cn.saytime.dao.impl;

import cn.saytime.bean.User;
import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class UserMapper implements cn.saytime.dao.UserMapper {

    @Resource
    protected SessionFactory sessionFactory;

    @Override
    public User getUserById(Integer id) {
        Session session = sessionFactory.openSession();
        //开启事务
//        Transaction tx = session.beginTransaction();
//        String sql = "select * from User where id =?";
        User user = session.get(User.class,id);
        session.close();
        return user;
    }

    @Override
    public List<User> getUserList() {
        Session session = sessionFactory.openSession();
        String hql = "from User";
        Query query = session.createQuery(hql);
        List<User> users = query.list();
        session.close();
        return users;
    }

    @Override
    public int add(User user) {
        Session session = sessionFactory.openSession();
        //开启事务
        Transaction tx = session.beginTransaction();
        tx.begin();
        session.save(user);
        tx.commit();
        session.close();
        return 1;
    }

    @Override
    public int update(Integer id, User user) {
        Session session = sessionFactory.openSession();
        //开启事务
        Transaction tx = session.beginTransaction();
//        String updateHql = "update User ";
        tx.begin();
        session.update(user);
        tx.commit();
        session.close();
        return 1;
    }

    @Override
    public int delete(Integer id) {
        Session session = sessionFactory.openSession();
        //开启事务
        Transaction tx = session.beginTransaction();
        User user = session.get(User.class,id);
        tx.begin();
        session.delete(user);
        tx.commit();
        session.close();
        return 0;
    }
}
