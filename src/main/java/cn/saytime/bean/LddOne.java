package cn.saytime.bean;

import lombok.Data;

@Data
public class LddOne {
    private Integer id;

    private String name;

    private Integer age;

    private String address;
}