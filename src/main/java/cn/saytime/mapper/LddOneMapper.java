package cn.saytime.mapper;

import cn.saytime.bean.LddOne;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LddOneMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LddOne record);

    int insertSelective(LddOne record);

    LddOne selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LddOne record);

    int updateByPrimaryKey(LddOne record);
}