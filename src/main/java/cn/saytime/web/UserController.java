package cn.saytime.web;

import cn.saytime.bean.JsonResult;
import cn.saytime.bean.User;
import cn.saytime.service.UserService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/")
public class UserController {

    private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    /**
     * 根据ID查询用户
     * @param id
     * @return
     */
    @RequestMapping(value = "user/{id}", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getUserById (@PathVariable(value = "id") Integer id){
        JsonResult jsonResult = new JsonResult();
        try {
            User user = userService.getUserById(id);
            jsonResult.setResult(user);
            jsonResult.setStatus("ok");
            LOGGER.info("查询成功###"+user.getUsername());
            LOGGER.error("查询成功###"+user.getUsername());
        } catch (Exception e) {
            jsonResult.setResult(e.getClass().getName() + ":" + e.getMessage());
            jsonResult.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询用户列表
     * @return
     */
    @RequestMapping(value = "users", method = RequestMethod.GET)
    public ResponseEntity<JsonResult> getUserList (){
        JsonResult jsonResult = new JsonResult();
        try {
            List<User> users = userService.getUserList();
            jsonResult.setResult(users);
            jsonResult.setStatus("okkkkkkkk");
        } catch (Exception e) {
            jsonResult.setResult(e.getClass().getName() + ":" + e.getMessage());
            jsonResult.setStatus("error");
            e.printStackTrace();
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 添加用户
     * @param user
     * @return
     */
    @RequestMapping(value = "user", method = RequestMethod.POST)
    public ResponseEntity<JsonResult> add (@RequestBody User user){
        JsonResult jsonResult = new JsonResult();
        try {
            int orderId = userService.add(user);
            if (orderId < 0) {
                jsonResult.setResult(orderId);
                jsonResult.setStatus("fail");
            } else {
                jsonResult.setResult(orderId);
                jsonResult.setStatus("ok");
            }
        } catch (Exception e) {
            jsonResult.setResult(e.getClass().getName() + ":" + e.getMessage());
            jsonResult.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    @RequestMapping(value = "user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<JsonResult> delete (@PathVariable(value = "id") Integer id){
        JsonResult jsonResult = new JsonResult();
        try {
            int ret = userService.delete(id);
            if (ret < 0) {
                jsonResult.setResult(ret);
                jsonResult.setStatus("fail");
            } else {
                jsonResult.setResult(ret);
                jsonResult.setStatus("ok");
            }
        } catch (Exception e) {
            jsonResult.setResult(e.getClass().getName() + ":" + e.getMessage());
            jsonResult.setStatus("error异常");

            e.printStackTrace();
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据id修改用户信息
     * @param user
     * @return
     */
    @RequestMapping(value = "user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<JsonResult> update (@PathVariable("id") Integer id, @RequestBody User user){
        JsonResult jsonResult = new JsonResult();
        try {
            int ret = userService.update(id, user);
            if (ret < 0) {
                jsonResult.setResult(ret);
                jsonResult.setStatus("fail");
            } else {
                jsonResult.setResult(ret);
                jsonResult.setStatus("ok");
            }
        } catch (Exception e) {
            jsonResult.setResult(e.getClass().getName() + ":" + e.getMessage());
            jsonResult.setStatus("error");

            e.printStackTrace();
        }
        return ResponseEntity.ok(jsonResult);
    }
}
